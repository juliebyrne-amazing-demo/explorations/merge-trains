# Merge Trains

Project to demonstrate the various cases of merge train behavior

## Approach

I'm using changes directly to the .gitlab-ci.yml file with echo statements in different jobs to let us know what is being run.  

*Note: Merge Trains have nothing to do with changing pipeline files and I am only using this approach so that I can easily determine from a set of pipeline log files which sets of changes were included in that particular pipeline, so I don't have to actually look at a running app in order to determine this.*

## Test Cases and Results

### MRs for feature A, B, and C have different changes that all get merged successfully at the same time

A Feature has in the *build1 job* an echo "change for A Feature" (I orignally had "change for MRX" but I changed this when I realized that it would simplify the process for future test cases); similarly, Feature B has in the *test1 job* an echo "change for B Feature", and Feature C has in the *deploy1 job* "change for C Feature".

*RESULT*: We can see that the pipeline for Feature A contains only the changes for Feature A ("change for MR9" echoed in build1 job); Feature B merge train pipeline contains the changes for Feature A and Feature B ("change for MR9" echoed in build1 job and "change for MR10" echoed in test1 job); Feature C merge train pipeline contains the changes for Features A, B, and C ("change for MR9" echoed in build1 job; "change for MR10" echoed in test1 job; "change for MR10" echoed in deploy1 job)

*EXAMPLE* - see MR's !9, !10, and !11 for the above test cases

### MR's for feature A and feature C have a conflict but B has changes that don't conflict
A Feature has in the *build1 job* an echo "change for A Feature" (I orignally had "change for MRX" but I changed this when I realized that it would simplify the process for future test cases); similarly, Feature B has in the *test1 job* an echo "change for B Feature", and Feature C has in the *build1 job* "change for C Feature".  The same line is changed as in the A Feature so this will cause a conflict.

*RESULT*: We can see that the pipeline for Feature A contains only the changes for Feature A ("change for MR9" echoed in build1 job); Feature B merge train pipeline contains the changes for Feature A and Feature B ("change for MR9" echoed in build1 job and "change for MR10" echoed in test1 job); Feature C MR is removed from the merge train and the user is prompted to merge.  After merging, the MR can be added back to the merge train.

*EXAMPLE* - see MR's !12, !13, and !14 for the above test case

### MR for A, B, and C when the merge pipeline for B fails
A Feature has in the *build1 job* an echo "change for A Feature" (I orignally had "change for MRX" but I changed this when I realized that it would simplify the process for future test cases); Feature B has added an additional job test2; the *test1 job* uses rules to output "exit 1" when the pipeline is a merge train pipeline; this causes a failure.  The test2 job echos "feature C" for a detached pipeline. Feature C has in the *deploy1 job* "change for C Feature". 

*RESULT*:  We can see that the pipeline for Feature A succeeds and only contains the changes for Feature A ("chagne for Feature A" echoed in build1 job).  The pipeline for Feature B fails, and while the Feature C pipeline was added to the merge train, it was removed because of the B failure.  These are the messages displayed in the MR for Feature C:


*EXAMPLE* - see MR's !24, !25, and !26 for the above test case

